package com.twuc.bagSaving;

import java.util.List;

public class StupidAssistant{
    private List<Cabinet> cabinets;
    private int saveCount;
    private int currentCabinetIndex = 0;

    public StupidAssistant(List<Cabinet> cabinets) {
        this.cabinets = cabinets;
    }

    public List<Cabinet> getCabinets() {
        return cabinets;
    }

    public int getSaveCount() {
        return saveCount;
    }

    public LockerSize validateLockerSize(Bag bag){
        LockerSize lockerSize = LockerSize.BIG;
        if(bag.getBagSize().getSizeNumber()<=20){
            lockerSize = LockerSize.SMALL;
        }else if(bag.getBagSize().getSizeNumber()<=30){
            lockerSize = LockerSize.MEDIUM;
        }else if(bag.getBagSize().getSizeNumber()<=40){
            lockerSize = LockerSize.BIG;
        }
        return lockerSize;
    }

    public Ticket save(Bag bag) {
        LockerSize lockerSize = validateLockerSize(bag);
        this.saveCount++;
        Ticket ticket = cabinets.get(currentCabinetIndex).save(bag, lockerSize);
        if(this.saveCount+1>=cabinets.get(currentCabinetIndex).getLockers().get(lockerSize).getCapacity()){
            this.saveCount = 0;
            if(currentCabinetIndex<cabinets.size()-1){
                this.currentCabinetIndex++;
            }
        }
        return ticket;
    }

    public Bag getBag(Ticket ticket){
        return cabinets.get(currentCabinetIndex).getBag(ticket);
    }
}
