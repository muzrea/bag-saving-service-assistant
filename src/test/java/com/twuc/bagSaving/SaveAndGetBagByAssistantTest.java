package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

public class SaveAndGetBagByAssistantTest {
    @Test
    void should_get_a_ticket_when_save_bag_by_assistant() {
        List<Cabinet> cabinets = getCabinets();
        StupidAssistant assistant = new StupidAssistant(cabinets);

        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM));

        assertNotNull(ticket);
    }

    private List<Cabinet> getCabinets() {
        Cabinet cabinet = new Cabinet(
                new LockerSetting(LockerSize.BIG, 1),
                new LockerSetting(LockerSize.MEDIUM, 3),
                new LockerSetting(LockerSize.SMALL, 3));

        Cabinet cabinet2 = new Cabinet(
                new LockerSetting(LockerSize.BIG, 3),
                new LockerSetting(LockerSize.MEDIUM, 3),
                new LockerSetting(LockerSize.SMALL, 3));
        Cabinet cabinet3 = new Cabinet(
                new LockerSetting(LockerSize.BIG, 3),
                new LockerSetting(LockerSize.MEDIUM, 3),
                new LockerSetting(LockerSize.SMALL, 3));
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(cabinet);
        cabinets.add(cabinet2);
        cabinets.add(cabinet3);
        return cabinets;
    }

    @Test
    void should_get_bag_by_assistant() {
        List<Cabinet> cabinets = getCabinets();
        StupidAssistant assistant = new StupidAssistant(cabinets);
        Bag bag1 = new Bag(BagSize.MEDIUM);

        Ticket ticket = assistant.save(bag1);
        Bag bag = assistant.getBag(ticket);

        assertEquals(bag1, bag);
    }

    @Test
    void should_save_at_same_size() {
        List<Cabinet> cabinets = getCabinets();
        StupidAssistant assistant = new StupidAssistant(cabinets);
        Bag smallerBag = new Bag(BagSize.SMALL);

        Ticket smallTicket = assistant.save(smallerBag);
        Bag getSmallBag = cabinets.get(0).getLockers().get(LockerSize.SMALL).getBag(smallTicket);

        assertSame(smallerBag, getSmallBag);
    }

    @Test
    void should_save_bag_wrong_when_over_capacity() {
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(new Cabinet(new LockerSetting(LockerSize.SMALL,1)));
        StupidAssistant assistant = new StupidAssistant(cabinets);
        Bag smallerBag = new Bag(BagSize.SMALL);
        Bag bag2 = new Bag(BagSize.SMALL);
        Ticket smallTicket = assistant.save(smallerBag);

        assertThrows(InsufficientLockersException.class,() -> assistant.save(bag2));
    }

    @Test
    void should_save_bags_in_order_by_assistant() {
        List<Cabinet> cabinets = getCabinets();
        StupidAssistant assistant = new StupidAssistant(cabinets);
        Bag bigBag = new Bag(BagSize.BIG);
        Ticket bigTicket = assistant.save(bigBag);
        Bag getBigBag = cabinets.get(0).getBag(bigTicket);

        assertEquals(bigBag, getBigBag);

        Bag bag = new Bag(BagSize.MEDIUM);
        Ticket ticket = assistant.save(bag);
        Bag bag1 = cabinets.get(1).getBag(ticket);

        assertEquals(bag1, bag);
    }

    /*@ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize"})
    void should_save_smaller_bag_to_smaller_locker(BagSize smallerBagSize, LockerSize smallerLockerSize) {
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(createCabinetWithPlentyOfCapacity());
        cabinets.add(createCabinetWithPlentyOfCapacity());
        StupidAssistant assistant = new StupidAssistant(cabinets);
        Bag smallerBag = new Bag(smallerBagSize);

        Ticket smallTicket = assistant.save(smallerBag);
        Bag getSmallBag = cabinets.get(0).getLockers().get(smallerLockerSize).getBag(smallTicket);

        assertSame(smallerBag, getSmallBag);
    }*/
}
